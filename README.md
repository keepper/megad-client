# megad-client

Php client based on curl for MegaD Devices (https://ab-log.ru/)

## CommandBuilder

Это набор классов помогающий собрать строку последовательности комманд, воспринимаемую контролером MegaD

### Использование

Простой пример использования


    <?php
    
    use Keepper\MegaD\Command\CommandBuilder;
    
    $builder = new CommandBuilder();
    
    $commandLine = $builder
        ->turnOff(1)        // Выключить прорт 1
        ->delay(10)         // Подождать 1 секунду
        ->turnOn(1)         // Включить порт 1
        ->reply(2)          // Повторить сценарий 2 раза 
        ->getResult();
        
    echo $commandLine;
    
    ?>
        
Вернет строку вида: **1:0;p10;1:1;r2**

### Поддержка проверки конфигурации

Если в экземпляр объекта CommandBuilder внедрить экземпляр объекта Controller то при вызове команд, CommandBuilder будет производить проверки

 * На наличие указанного порта
 * На его конфигурацию
 * На соответствие типа порта, вызываемой команде
 
В случае определении ошибок, будут сгенерированы соответствующие исключения. 

Пример использования

    <?php
    
    use Keepper\MegaD\Command\CommandBuilder;
    use Keepper\MegaD\Configuration\ConfigReader;
    
    $configReader = new ConfigReader();
    $builder = new CommandBuilder();
    $builder->checkConfiguration($configReader);
    
    try {
    
        echo $builder->turnOn(1)->getResult();
        
    } catch (\Keepper\MegaD\Exceptions\UnexistingPortException $e) {
    
        echo "Указанный порт не существует";
        
    } catch (\Keepper\MegaD\Exceptions\UnconfiguredPortException $e) {
    
        echo "Указанный порт не сконфигурирован";
        
    } catch (\Keepper\MegaD\Exceptions\NotOutputPortException $e) {
    
        echo "Указанный порт не является Выходом";
        
    }
    
    ?>