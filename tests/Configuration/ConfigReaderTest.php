<?php
namespace Keepper\MegaD\Tests\Configuration;

use Keepper\Lib\HttpClient\Interfaces\ConfiguredHttpClientInterface;
use Keepper\MegaD\Configuration\ConfigReader;
use Keepper\MegaD\Configuration\ConfigReaderInterface;
use Keepper\MegaD\Port\Port;
use Keepper\MegaD\Tests\Fixtures\ResponseFromFixtureTrait;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class ConfigReaderTest extends \PHPUnit_Framework_TestCase {

    use ResponseFromFixtureTrait;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ConfiguredHttpClientInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $httpClient;

    /**
     * @var MegaDeviceTransportInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $transport;

    /**
     * @var ConfigReaderInterface
     */
    private $reader;

    public function setUp() {
        parent::setUp();
        $this->logger = new Logger('Unit test');
        $this->httpClient = $this->getMockBuilder(ConfiguredHttpClientInterface::class)->getMock();
        $this->httpClient->method('withTimeOut')->willReturn($this->httpClient);
        $this->transport = $this->getMockBuilder(MegaDeviceTransportInterface::class)->getMock();
        $this->transport->method('httpLayer')->willReturn($this->httpClient);
        $this->reader = new ConfigReader(37, $this->transport);
        $this->reader->setLogger($this->logger);

    }

    /**
     * @expectedException \Keepper\MegaD\Exceptions\UnexistingPortException
     */
    public function testUnexistingPortException() {
        $this->reader->getPortConfiguration(new Port(40));
    }

    /**
     * @expectedException \Keepper\MegaD\Exceptions\UnconfiguredPortException
     */
    public function testUnconfiguredPortException() {
        $portNumber = rand(0,37);

        $this->transport
            ->expects($this->once())
            ->method('request')
            ->with(['pt' => $portNumber])
            ->willReturn($this->getResponseFromFixture('NotConfiguredPortPageResponse'));

        $this->reader->getPortConfiguration(new Port($portNumber));
    }

    /**
     * @expectedException \Keepper\MegaD\Exceptions\MegaRequestException
     */
    public function testMegaDeviceException() {
        $portNumber = rand(0,37);

        $this->transport
            ->expects($this->once())
            ->method('request')
            ->with(['pt' => $portNumber])
            ->willReturn($this->getResponseFromFixture('NotConfiguredPortPageIncorrectResponse'));

        $this->reader->getPortConfiguration(new Port($portNumber));
    }

    public function testGetInputPortConfiguration() {
        $portNumber = rand(0,37);
        $this->transport
            ->expects($this->any())
            ->method('request')
            ->with(['pt' => $portNumber])
            ->willReturn($this->getResponseFromFixture('InputPortPageResponse'));

        $configuration = $this->reader->getInputPortConfiguration(new Port($portNumber));

        $this->assertTrue($configuration->type()->isInput());
        $this->assertTrue($configuration->mode()->isPress());
    }

    public function testGetDsenPortConfiguration() {
        $portNumber = rand(0,37);
        $this->transport
            ->expects($this->any())
            ->method('request')
            ->with(['pt' => $portNumber])
            ->willReturn($this->getResponseFromFixture('Dsen1WbusPortPageResponse'));

        $configuration = $this->reader->getDsenPortConfiguration(new Port($portNumber));

        $this->assertTrue($configuration->type()->isDigitalSensor());
        $this->assertTrue($configuration->mode()->isW1Bus());
    }
}