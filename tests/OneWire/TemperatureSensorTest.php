<?php
namespace Keepper\MegaD\Tests\OneWire;

use Keepper\MegaD\OneWire\TemperatureSensor;

class TemperatureSensorTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForSensor
	 */
	public function testSensor($uuid, $temp, $expectedTemp) {
		$sensor = new TemperatureSensor($uuid, $temp);

		$this->assertEquals($uuid, $sensor->uuid());
		$this->assertEquals($expectedTemp, $sensor->getTemperature());
	}

	public function dataProviderForSensor() {
		return [
			['abc-1', 85.0, null],
			['abc-2', 30.6, 30.6],
		];
	}
}