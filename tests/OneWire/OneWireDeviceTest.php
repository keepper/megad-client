<?php
namespace Keepper\MegaD\Tests\OneWire;

use Keepper\MegaD\OneWire\OneWireDevice;

class OneWireDeviceTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider detaProviderForUuid
	 */
	public function testUuid($uuid) {
		$device = new OneWireDevice($uuid);
		$this->assertEquals($uuid, $device->uuid());
	}

	public function detaProviderForUuid() {
		return [
			['abc-'.rand(1000,20000)],
			['dce-'.rand(3000,4000)],
		];
	}
}