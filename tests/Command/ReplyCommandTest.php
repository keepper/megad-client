<?php
namespace Keepper\MegaD\Tests\Command;

use Keepper\MegaD\Command\ReplyCommand;

class ReplyCommandTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForCommand
	 */
	public function testCommand($count, $expectedResult) {
		$command = new ReplyCommand($count);

		$this->assertEquals($expectedResult, $command->getCommand());
		$this->assertEquals($expectedResult, (string) $command);
	}

	public function dataProviderForCommand() {
		return [
			[1, 'r1'],
			[2, 'r2'],
			[3, 'r3']
		];
	}
}