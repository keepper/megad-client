<?php
namespace Keepper\MegaD\Tests\Command;

use Keepper\MegaD\Command\AllOutputCommand;

class AllOutputCommandTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForCommand
	 */
	public function testCommand($cmd, $expectedResult) {
		$command = new AllOutputCommand($cmd);

		$this->assertEquals($expectedResult, $command->getCommand());
		$this->assertEquals($expectedResult, (string) $command);
	}

	public function dataProviderForCommand() {
		return [
			[AllOutputCommand::ON, 'a:1'],
			[AllOutputCommand::OFF, 'a:0'],
		];
	}
}