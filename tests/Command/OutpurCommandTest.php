<?php
namespace Keepper\MegaD\Tests\Command;

use Keepper\MegaD\Command\OutputCommand;
use Keepper\MegaD\Port\Port;

class OutputCommandTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForCommand
	 */
	public function testCommand($port, $cmd, $expectedResult) {
		$command = new OutputCommand(new Port($port), $cmd);

		$this->assertEquals($expectedResult, $command->getCommand());
		$this->assertEquals($expectedResult, (string) $command);
	}

	public function dataProviderForCommand() {
		return [
			[0, OutputCommand::ON, '0:1'],
			[10, OutputCommand::OFF, '10:0'],
			[20, OutputCommand::TOGGLE, '20:2'],
		];
	}
}