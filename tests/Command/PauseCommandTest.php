<?php
namespace Keepper\MegaD\Tests\Command;

use Keepper\MegaD\Command\PauseCommand;

class PauseCommandTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForCommand
	 */
	public function testCommand($delay, $expectedResult) {
		$command = new PauseCommand($delay);

		$this->assertEquals($expectedResult, $command->getCommand());
		$this->assertEquals($expectedResult, (string) $command);
	}

	public function dataProviderForCommand() {
		return [
			[1, 'p1'],
			[2, 'p2'],
			[3, 'p3']
		];
	}
}