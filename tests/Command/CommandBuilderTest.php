<?php
namespace Keepper\MegaD\Tests\Command;

use Keepper\MegaD\Command\AllOutputCommand;
use Keepper\MegaD\Command\CommandBuilder;
use Keepper\MegaD\Command\OutputCommand;
use Keepper\MegaD\Command\PauseCommand;
use Keepper\MegaD\Command\ReplyCommand;
use Keepper\MegaD\Configuration\ConfigReaderInterface;
use Keepper\MegaD\Device\MegaDeviceControllerInterface;
use Keepper\MegaD\Exceptions\UnconfiguredPortException;
use Keepper\MegaD\Exceptions\UnexistingPortException;
use Keepper\MegaD\Port\Port;
use Keepper\MegaD\Port\PortConfiguration;
use Keepper\MegaD\Port\PortTypeInterface;

class CommandBuilderTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @expectedException \Keepper\MegaD\Exceptions\BigComandException
	 */
	public function testLengthException() {
		$builder = new CommandBuilder();
		$beforeLength = 0;
		$length = 0;

		try {
			while(true) {
				$command = new OutputCommand(new Port(100), OutputCommand::OFF);
				$beforeLength = $length;
				$length += strlen($command) + 1;
				$builder->attachCommand($command);

				if ($length > 16) {
					print_r([$length]);
					break;
				}
			}
		} catch (\Exception $e) {
			$this->assertLessThan(15, $beforeLength);
			throw $e;
		}
	}

	public function testReply() {
		$cnt = rand(1,10);
		$cmd = new ReplyCommand($cnt);
		// Проверяем что добавляется нужная команда
		$builder = $this->getMockBuilder(CommandBuilder::class)->setMethods(['attachCommand'])->getMock();
		$builder
			->expects($this->once())
			->method('attachCommand')
			->with($cmd)
			->willReturn($builder);

		$builder->reply($cnt);

		$builder = new CommandBuilder();
		$builder->reply($cnt);

		$this->assertEquals($cmd->getCommand(), $builder->getResult());
	}

	public function testDelay() {
		$delay = rand(1,10);
		$cmd = new PauseCommand($delay);
		// Проверяем что добавляется нужная команда
		$builder = $this->getMockBuilder(CommandBuilder::class)->setMethods(['attachCommand'])->getMock();
		$builder
			->expects($this->once())
			->method('attachCommand')
			->with($cmd)
			->willReturn($builder);

		$builder->delay($delay);

		$builder = new CommandBuilder();
		$builder->delay($delay);

		$this->assertEquals($cmd->getCommand(), $builder->getResult());
	}

	/**
	 * @dataProvider dataProviderForThrowPortExceptions
	 * @expectedException \Keepper\MegaD\Exceptions\UnexistingPortException
	 */
	public function testThrowTryCommandByUnexistingPort($method) {
		$port = new Port(rand(0,40));

		$configReader = $this->getMockBuilder(ConfigReaderInterface::class)->getMock();

        $configReader->method('getPortConfiguration')->with($port)->willThrowException(new UnexistingPortException('Test unexists port'));
		$builder = new CommandBuilder();
		$builder->checkConfiguration($configReader);

		$builder->$method($port->number());
	}

	/**
	 * @dataProvider dataProviderForThrowPortExceptions
	 * @expectedException \Keepper\MegaD\Exceptions\UnconfiguredPortException
	 */
	public function testThrowTryCommandByUnconfiguredPort($method) {
		$port = new Port(rand(0,37));

        $configReader = $this->getMockBuilder(ConfigReaderInterface::class)->getMock();
        $configReader->method('getPortConfiguration')->with($port)->willThrowException(new UnconfiguredPortException('Test unconfigured port'));
		$builder = new CommandBuilder();
		$builder->checkConfiguration($configReader);

		$builder->$method($port->number());
	}

	/**
	 * @dataProvider dataProviderForThrowPortExceptions
	 * @expectedException \Keepper\MegaD\Exceptions\NotOutputPortException
	 */
	public function testThrowTryCommandByNotOutputPort($method) {
		$port = new Port(rand(0,37));

        $configReader = $this->getMockBuilder(ConfigReaderInterface::class)->getMock();
		$portConfig = new PortConfiguration(PortTypeInterface::IN);
        $configReader->method('getPortConfiguration')->with($port)->willReturn($portConfig);
		$builder = new CommandBuilder();
		$builder->checkConfiguration($configReader);

		$builder->$method($port->number());
	}

	/**
	 * @dataProvider dataProviderForThrowPortExceptions
	 */
	public function testNotThrowByOutputPort($method) {
		$port = new Port(rand(0,37));

        $configReader = $this->getMockBuilder(ConfigReaderInterface::class)->getMock();
		$portConfig = new PortConfiguration(PortTypeInterface::OUT);
        $configReader->method('getPortConfiguration')->with($port)->willReturn($portConfig);
		$builder = new CommandBuilder();
		$builder->checkConfiguration($configReader);

		$builder->$method($port->number());
	}

	public function dataProviderForThrowPortExceptions() {
		return [
			['turnOn'],
			['turnOff'],
			['outputToggle']
		];
	}

	/**
	 * @dataProvider dataProviderForTurn
	 */
	public function testTurn($method, $value) {
		$port = new Port(rand(0,40));
		$cmd = new OutputCommand($port, $value);
		// Проверяем что добавляется нужная команда
		$builder = $this->getMockBuilder(CommandBuilder::class)->setMethods(['attachCommand'])->getMock();
		$builder
			->expects($this->once())
			->method('attachCommand')
			->with($cmd)
			->willReturn($builder);

		$builder->$method($port->number());
	}

	public function dataProviderForTurn() {
		return [
			['turnOn', OutputCommand::ON],
			['turnOff', OutputCommand::OFF],
			['outputToggle', OutputCommand::TOGGLE]
		];
	}

	/**
	 * @dataProvider dataProviderForAllTurn
	 */
	public function testAllTurn($method, $value) {
		$port = new Port(rand(0,40));
		$cmd = new AllOutputCommand($value);
		// Проверяем что добавляется нужная команда
		$builder = $this->getMockBuilder(CommandBuilder::class)->setMethods(['attachCommand'])->getMock();
		$builder
			->expects($this->once())
			->method('attachCommand')
			->with($cmd)
			->willReturn($builder);

		$builder->$method($port->number());
	}

	public function dataProviderForAllTurn() {
		return [
			['allTurnOn', AllOutputCommand::ON],
			['allTurnOff', AllOutputCommand::OFF]
		];
	}

	public function TestGetResult() {
		$builder = new CommandBuilder();

		$commandLine = $builder
			->turnOff(1)
			->delay(10)
			->turnOn(1)
			->reply(2)
			->getResult();

		$this->assertEquals('1:0;p10;1:1;r2', $commandLine);
	}
}