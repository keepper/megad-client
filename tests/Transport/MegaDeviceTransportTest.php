<?php
namespace Keepper\MegaD\Tests\Transport;

use Keepper\Lib\HttpClient\Interfaces\ConfiguredHttpClientInterface;
use Keepper\Lib\HttpClient\Interfaces\HttpClientInterface;
use Keepper\MegaD\Exceptions\MegaRequestException;
use Keepper\MegaD\Transport\MegaDeviceTransport;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Monolog\Logger;
use Phly\Http\Response;

class MegaDeviceTransportTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var MegaDeviceTransportInterface
	 */
	private $transport;

	/**
	 * @var ConfiguredHttpClientInterface|\PHPUnit_Framework_MockObject_MockObject
	 */
	private $httpClient;

	public function setUp(){
		parent::setUp();
		$logger = new Logger('UnitTest');
		$this->httpClient = $this->getMockBuilder(ConfiguredHttpClientInterface::class)->getMock();
		$this->httpClient->method('withTimeOut')->willReturn($this->httpClient);
		$this->transport = new MegaDeviceTransport('192.168.0.14', 'sec', $this->httpClient);
		$this->transport->setLogger($logger);
	}

	/**
	 * @dataProvider dataProviderForApiUrl
	 */
	public function testApiUr($ip, $pwd, $expectedUrl) {
		$transport = new MegaDeviceTransport($ip, $pwd);
		$this->assertEquals($expectedUrl, $transport->apiUrl());
	}

	public function dataProviderForApiUrl() {
		return [
			['192.168.0.14', 'sec', 'http://192.168.0.14/sec/'],
			['234.230.10.0', 'abc', 'http://234.230.10.0/abc/'],
		];
	}

	public function testHttpLayer() {
		$this->assertEquals($this->httpClient, $this->transport->httpLayer());

		$transport = new MegaDeviceTransport();
		$this->assertInstanceOf(HttpClientInterface::class, $transport->httpLayer());
	}

	public function testExceptionOnRequest() {
		$this->httpClient->method('get')->willThrowException(new \Exception('Типа ошибка'));

		try {
			$this->transport->request([]);
			$this->assertTrue(false, 'Ожидали исключение');
		} catch (MegaRequestException $e) {
			$this->assertNull($e->httpException());
		}
	}

	/**
	 * @expectedException Keepper\MegaD\Exceptions\MegaRequestException
	 */
	public function testExceptionOnNot200Answer() {
		$params = ['a' => 1, 'b' => rand(1,100)];
		$this->httpClient
			->expects($this->once())
			->method('get')
			->with($this->transport->apiUrl(), $params)
			->willReturn(new Response('php://memory', 500));


		$this->transport->request($params);
	}

	public function testCachedResult() {
		$params = ['a' => 1, 'b' => rand(1,100)];
		$response = new Response('php://memory', 200);
		$response->getBody()->write('test');

		$this->httpClient
			->expects($this->once())
			->method('get')
			->with($this->transport->apiUrl(), $params)
			->willReturn($response);


		$result = $this->transport->request($params);
		$this->assertEquals($response, $result);

		// Повторный запрос не должен обращаться к серверу
		$result = $this->transport->request($params);
		$this->assertEquals($response, $result);
	}
}