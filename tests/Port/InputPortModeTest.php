<?php
namespace Keepper\MegaD\Tests\Port;

use Keepper\MegaD\Port\InputPortMode;

class InputPortModeTest extends \PHPUnit_Framework_TestCase {
	/**
	 * @dataProvider dataProviderForValue
	 */
	public function testValue($value, $isPress, $isReleas, $isPR, $isClick) {
		$mode = new InputPortMode($value);

		$this->assertEquals($value, (string) $mode, 'Ожидали, что при реобразовании к строке, получем значение режима входного порта');
		$this->assertEquals($isPress, $mode->isPress());
		$this->assertEquals($isReleas, $mode->isRelease());
		$this->assertEquals($isPR, $mode->isPressAndRelease());
		$this->assertEquals($isClick, $mode->isClick());
	}

	public function dataProviderForValue() {
		return [
			[InputPortMode::PRESS, true, false, false, false],
			[InputPortMode::RELEASE, false, true, false, false],
			[InputPortMode::PRESS_AND_RELEASE, false, false, true, false],
			[InputPortMode::CLICK, false, false, false, true],
		];
	}
}