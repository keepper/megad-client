<?php
namespace Keepper\MegaD\Tests\Port;

use Keepper\MegaD\Port\Port;

class PortTest extends \PHPUnit_Framework_TestCase {

	public function testNumber() {
		$number = rand(0,60);
		$port = new Port($number);
		$this->assertEquals($number, $port->number());
	}
}