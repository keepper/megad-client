<?php
namespace Keepper\MegaD\Tests\Port;

use Keepper\MegaD\Port\InputPortConfiguration;
use Keepper\MegaD\Port\InputPortModeInterface;

class InputPortConfigurationTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForDefaultProperty
	 */
	public function testDefaultProperty($mode) {
		$input = new InputPortConfiguration($mode);

		$this->assertTrue($input->type()->isInput());
		$this->assertInstanceOf(InputPortModeInterface::class, $input->mode());
		$this->assertEquals($mode, (string) $input->mode());

		$this->assertEquals('', $input->action());
		$this->assertEquals('', $input->postNetworkRequestAction());
		$this->assertFalse($input->isAlwaysCallPost());
		$this->assertFalse($input->isAlwaysCallAction());
		$this->assertFalse($input->isAlwayseP_R());
		$this->assertFalse($input->isOffBounceProtection());
	}

	public function dataProviderForDefaultProperty() {
		return [
			[InputPortModeInterface::PRESS],
			[InputPortModeInterface::RELEASE],
			[InputPortModeInterface::PRESS_AND_RELEASE],
			[InputPortModeInterface::CLICK],
		];
	}

	/**
	 * @dataProvider dataProviderForProperty
	 */
	public function testProperty($act, $net, $isAct, $isNet, $isPR, $offProtection) {
		$input = new InputPortConfiguration(InputPortModeInterface::PRESS, $act,	$net, $isPR, $isAct, $isNet, $offProtection);

		$this->assertEquals($act, $input->action());
		$this->assertEquals($net, $input->postNetworkRequestAction());
		$this->assertEquals($isNet, $input->isAlwaysCallPost());
		$this->assertEquals($isAct, $input->isAlwaysCallAction());
		$this->assertEquals($isPR, $input->isAlwayseP_R());
		$this->assertEquals($offProtection, $input->isOffBounceProtection());
	}

	public function dataProviderForProperty() {
		return [
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), true, false, false, false],
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), true, true, false, false],
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), true, false, true, false],
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), true, false, false, true],
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), false, false, true, false],
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), false, true, false, false],
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), false, true, true, false],
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), false, true, true, true],
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), true, true, true, true],
			['some-act-'.rand(1,100), 'some-net-'.rand(1,100), true, true, true, false],
		];
	}
}