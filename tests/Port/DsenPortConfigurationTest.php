<?php
namespace Keepper\MegaD\Tests\Port;

use Keepper\MegaD\Port\DsenPortConfiguration;
use Keepper\MegaD\Port\DsenPortModeInterface;

class DsenPortConfigurationTest extends \PHPUnit_Framework_TestCase {
	/**
	 * @dataProvider dataProviderForDefaultProperty
	 */
	public function testDefaultProperty($mode) {
		$dsen = new DsenPortConfiguration($mode);

		$this->assertTrue($dsen->type()->isDigitalSensor());
		$this->assertInstanceOf(DsenPortModeInterface::class, $dsen->mode());
		$this->assertEquals($mode, (string) $dsen->mode());
	}

	public function dataProviderForDefaultProperty() {
		return [
			[DsenPortModeInterface::DHT22],
			[DsenPortModeInterface::W1],
			[DsenPortModeInterface::W1BUS],
			[DsenPortModeInterface::W26],
			[DsenPortModeInterface::IB],
		];
	}
}