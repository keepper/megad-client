<?php
namespace Keepper\MegaD\Tests\Port;

use Keepper\MegaD\Port\DsenPortMode;
use Keepper\MegaD\Port\DsenPortModeInterface;

class DsenPortModeTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForIsMethods
	 */
	public function testIsMethods($value, $isDHT22, $isW1, $isW1Bus, $isIButton, $isW26) {
		$mode = new DsenPortMode($value);

		$this->assertEquals($value, (string) $mode);
		$this->assertEquals($isDHT22, $mode->isDHT22());
		$this->assertEquals($isW1, $mode->isW1());
		$this->assertEquals($isW1Bus, $mode->isW1Bus());
		$this->assertEquals($isIButton, $mode->isIButton());
		$this->assertEquals($isW26, $mode->isW26());
	}

	public function dataProviderForIsMethods() {
		return [
			[DsenPortModeInterface::DHT22, true, false, false, false, false],
			[DsenPortModeInterface::W1, false, true, false, false, false],
			[DsenPortModeInterface::W1BUS, false, false, true, false, false],
			[DsenPortModeInterface::IB, false, false, false, true, false],
			[DsenPortModeInterface::W26, false, false, false, false, true],
		];
	}

}