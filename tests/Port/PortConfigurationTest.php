<?php
namespace Keepper\MegaD\Tests\Port;

use Keepper\MegaD\Port\PortConfiguration;
use Keepper\MegaD\Port\PortTypeInterface;

class PortConfigurationTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForType
	 */
	public function testType($value) {
		$configuration = new PortConfiguration($value);

		$this->assertInstanceOf(PortTypeInterface::class, $configuration->type());
		$this->assertEquals($value, (string) $configuration->type());
	}

	public function dataProviderForType() {
		return [
			[PortTypeInterface::IN],
			[PortTypeInterface::ADC],
			[PortTypeInterface::OUT],
			[PortTypeInterface::DSEN],
			['some-value-'.rand(1,100)],
		];
	}
}