<?php
namespace Keepper\MegaD\Tests\Port;

use Keepper\MegaD\Port\PortType;

class PortTypeTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForValue
	 */
	public function testValue($value, $isIn, $isOut, $isDsen, $isAdc, $isI2C, $isNc) {
		$type = new PortType($value);

		$this->assertEquals($value, (string) $type, 'Ожидали, что при реобразовании к строке, получем значение типа порта');
		$this->assertEquals($isIn, $type->isInput());
		$this->assertEquals($isOut, $type->isOutput());
		$this->assertEquals($isDsen, $type->isDigitalSensor());
		$this->assertEquals($isAdc, $type->isAnalogDigitalConverter());
	}

	public function dataProviderForValue() {
		return [
			[PortType::IN, true, false, false, false, false, false],
			[PortType::OUT, false, true, false, false, false, false],
			[PortType::DSEN, false, false, true, false, false, false],
			[PortType::ADC, false, false, false, true, false, false],
			[PortType::I2C, false, false, false, false, true, false],
			[PortType::NC, false, false, false, false, false, true],
		];
	}
}