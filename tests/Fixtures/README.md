# Fixtures

Ответы устройства мега, на различный запросы

## Запрос страницы конфигурации

url: http://mega.home/sec/?cf=1
fixture: ConfigPageResponse.fixture

## Запрос страницы конфигурации Megad-ID

url: http://mega.home/sec/?cf=2
fixture: ConfigMegadIdPageResponse.fixture

## Запрос страницы конфигурации Cron

url: http://mega.home/sec/?cf=7
fixture: ConfigCronPageResponse.fixture

## Запрос страницы конфигурации Program

url: http://mega.home/sec/?cf=9
fixture: ConfigProgrammPageResponse.fixture

## Запрос страницы порта XP1

url: http://mega.home/sec/?cf=3
fixture: Xp1Response.fixture

Что ответит если к порту не подключено исполнительное устройство

## Запрос не сконфигурированного Входного порта

url: http://mega.home/sec/?pt=2
fixture: NotConfiguredPortPageResponse.fixture

## Запрос страницы порта сконфигурированного как Вход

url: http://mega.home/sec/?pt=2
fixture: InputPortPageResponse.fixture

## Запрос страницы порта сконфигурированного как Выход

url: http://mega.home/sec/?pt=2
fixture: OutputPortPageResponse.fixture

## Запрос страницы порта сконфигурированного как Dsen

url: http://mega.home/sec/?pt=2
fixture: DsenPortPageResponse.fixture

## Запрос страницы порта сконфигурированного как DSen в режиме 1WBUS

url: http://mega.home/sec/?pt=2
fixture: Dsen1WbusPortPageResponse.fixture

## Запрос списка 1w сенсоров на шине 1wbus

url: http://mega.home/sec/?pt=2&cmd=list
fixture: Dsen1WbusDeviceListResponse.fixture


## Запрос страницы порта сконфигурированного как I2C

url: http://mega.home/sec/?pt=2
fixture: I2CPortPageResponse.fixture

## Запрос страницы порта сконфигурированного как ADC

url: http://mega.home/sec/?pt=2
fixture: ADCPortPageResponse.fixture

## Ответ на запрос конфигурирования порта

fixture: SetCPortConfigurationResponse.fixture

