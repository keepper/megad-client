<?php
namespace Keepper\MegaD\Tests\Fixtures;


use Phly\Http\Response;
use Psr\Http\Message\ResponseInterface;

trait ResponseFromFixtureTrait {

    protected function getResponseFromFixture(string $fixtureName): ResponseInterface {
        $fixture = file_get_contents(__DIR__.'/../Fixtures/'.$fixtureName.'.fixture');
        $fixture = explode("\n", $fixture);
        $statusCode = array_shift($fixture);
        $headers = [];
        $body = '';
        $isBody = false;

        while(true) {
            if (count($fixture) == 0) {
                break;
            }

            $line = array_shift($fixture);
            if ($line == '---') {
                $isBody = true;
                continue;
            }

            if ($isBody) {
                $body .= $line;
                continue;
            }

            $headers[] = $line;
        }

        $response = new Response('php://memory', $statusCode);
        $response->getBody()->write($body);
        foreach ($headers as $header) {
            $parts = explode(':', $header);
            $response->withHeader($parts[0], $parts[1]);
        }
        return $response;
    }
}