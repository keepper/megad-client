<?php
namespace Keepper\MegaD\Tests\Integration;

use Keepper\Lib\Curl\Curl;
use Keepper\Lib\Curl\Option\OptionInterface;
use Keepper\Lib\HttpClient\HttpClient;
use Keepper\MegaD\Device\MegaDeviceController;
use Keepper\MegaD\Transport\MegaDeviceTransport;
use Monolog\Logger;

class IntegrationRealTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var MegaDeviceController
     */
    private $megaDevice;

    public function setUp() {
        parent::setUp();
        $httpClient = new HttpClient(null, new Logger('Http'));
        $transport = new MegaDeviceTransport('192.168.0.14', 'sec', $httpClient);
        $transport->setLogger(new Logger('Transport'));

        $this->megaDevice = new MegaDeviceController(
            $transport
        );

        $this->megaDevice->setLogger(new Logger('Real'));
    }

    /**
     * @group integration
     */
    public function testGetDeviceTime() {
        $this->megaDevice->getDeviceTime()->format('H:i:s');
    }

    /**
     * @group integration
     */
    public function testSetTime() {
        $now = new \DateTime('now', new \DateTimeZone('Asia/Krasnoyarsk'));
        $this->megaDevice->setTime($now);
    }

    /**
     * @group integration
     */
    public function testReadFromOneWireBus() {
        $temp1 = 'ff563e501704';
        $temp2 = 'ffae58501704';
        $devices = $this->megaDevice->getW1bus(0)->getDevices();

        $this->assertTrue($this->megaDevice->getW1bus(0)->hasDevice($temp1));
        $this->assertTrue($this->megaDevice->getW1bus(0)->hasDevice($temp2));

        $v1 = $this->megaDevice->getW1bus(0)->getTemperature($temp1)->getTemperature();
        $v2 = $this->megaDevice->getW1bus(0)->getTemperature($temp2)->getTemperature();
        print_r([$v1, $v2]);
    }
}