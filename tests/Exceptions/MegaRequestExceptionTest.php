<?php
namespace Keepper\MegaD\Tests\Exceptions;

use Keepper\Lib\Curl\Exceptions\CurlException;
use Keepper\MegaD\Exceptions\MegaRequestException;

class MegaRequestExceptionTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @dataProvider dataProviderForHttpException
	 */
	public function testHttpException($prevException, $expectedResult) {
		$e = new MegaRequestException('', 0, $prevException);

		$this->assertEquals($expectedResult, $e->httpException());
	}

	public function dataProviderForHttpException() {
		$curlException = new CurlException('Some curl lib exception');
		return [
			[new \Exception('Some exceptions'), null],
			[$curlException, $curlException]
		];
	}
}