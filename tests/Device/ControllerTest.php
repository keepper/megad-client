<?php
namespace Keepper\MegaD\Tests\Device;

use Keepper\Lib\HttpClient\Interfaces\ConfiguredHttpClientInterface;
use Keepper\MegaD\Configuration\ConfigReaderInterface;
use Keepper\MegaD\Device\MegaDeviceController;
use Keepper\MegaD\Device\MegaDeviceControllerInterface;
use Keepper\MegaD\Port\Port;
use Keepper\MegaD\Tests\Fixtures\ResponseFromFixtureTrait;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Monolog\Logger;
use Phly\Http\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class ControllerTest extends \PHPUnit_Framework_TestCase {

    use ResponseFromFixtureTrait;
	/**
	 * @var LoggerInterface
	 */
	private $logger;

	/**
	 * @var ConfiguredHttpClientInterface|\PHPUnit_Framework_MockObject_MockObject
	 */
	private $httpClient;

    /**
     * @var MegaDeviceTransportInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $transport;

    /**
     * @var ConfigReaderInterface|\PHPUnit_Framework_MockObject_MockObject
     */
    private $configReader;

	/**
	 * @var MegaDeviceControllerInterface
	 */
	private $mega;

	public function setUp() {
		parent::setUp();
		$this->logger = new Logger('Unit test');
		$this->httpClient = $this->getMockBuilder(ConfiguredHttpClientInterface::class)->getMock();
		$this->httpClient->method('withTimeOut')->willReturn($this->httpClient);
        $this->transport = $this->getMockBuilder(MegaDeviceTransportInterface::class)->getMock();
        $this->transport->method('httpLayer')->willReturn($this->httpClient);
        $this->configReader = $this->getMockBuilder(ConfigReaderInterface::class)->getMock();
		$this->mega = new MegaDeviceController($this->transport, $this->configReader);
		$this->mega->setLogger($this->logger);
	}

	public function testRunDefaultAction() {
		$portNumber = rand(0, 37);
		$this->transport
			->method('request')
			->willReturnMap([
				[['pt' => $portNumber], $this->getResponseFromFixture('InputPortPageResponse')],
				[['pt' => $portNumber, 'cmd' => 'd'], new Response()]
			]);

		$this->mega->runDefaultAction(new Port($portNumber));
	}

	public function testSaveState() {
		$this->transport->expects($this->once())
			->method('request')
			->with(['cmd' => 's'])
			->willReturn(new Response());

		$this->mega->saveState();
	}

	public function testSetTime() {
		$date = \DateTime::createFromFormat('Y-m-d H:i:s', '2019-01-17 23:15:30');
		$this->transport->expects($this->once())
			->method('request')
			->with(['cf' => 7, 'stime' => '23:15:30:0'])
			->willReturn(new Response());

		$this->mega->setTime($date);
	}

	public function testGetTime() {
		$this->transport->expects($this->once())
			->method('request')
			->with(['cf' => 7])
			->willReturn($this->getResponseFromFixture('ConfigCronPageResponse'));

		$deviceTime = $this->mega->getDeviceTime();

		$this->assertEquals('22:54:17', $deviceTime->format('H:i:s'));
	}


}