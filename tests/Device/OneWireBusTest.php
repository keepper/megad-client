<?php
namespace Keepper\MegaD\Tests\Device;

use Keepper\Lib\HttpClient\Exceptions\ResponseFormatException;
use Keepper\MegaD\Device\OneWireBus;
use Keepper\MegaD\Device\OneWireBusInterface;
use Keepper\MegaD\Exceptions\MegaRequestException;
use Keepper\MegaD\Exceptions\OneWireDeviceTypeException;
use Keepper\MegaD\OneWire\OneWireDevice;
use Keepper\MegaD\OneWire\TemperatureSensor;
use Keepper\MegaD\Port\Port;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Phly\Http\Response;

class OneWireBusTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var MegaDeviceTransportInterface|\PHPUnit_Framework_MockObject_MockObject
	 */
	private $transport;

	/**
	 * @var OneWireBusInterface
	 */
	private $bus;

	public function setUp() {
		parent::setUp();
		$port = rand(0,30);
		$this->transport = $this->getMockBuilder(MegaDeviceTransportInterface::class)->getMock();
		$this->bus = new OneWireBus(new Port($port), $this->transport);

		$response = new Response();
		$response->getBody()->write('ff563e501704:7.93;ffae58501704:4.68;ffae58591704');

		$this->transport
			->method('request')
			->with(['pt'=> $port, 'cmd' => 'list'])
			->willReturn($response);
	}

	public function testGetDevices() {
		$devices = $this->bus->getDevices();

		$expected = [
			new TemperatureSensor('ff563e501704', 7.93),
			new TemperatureSensor('ffae58501704', 4.68),
			new OneWireDevice('ffae58591704')
		];
		$this->assertEquals($expected, $devices);
	}

	/**
	 * @dataProvider dataProviderForHasDevice
	 */
	public function testHasDevice($uuid, $expectedResult) {
		$this->assertEquals($expectedResult, $this->bus->hasDevice($uuid));
	}

	public function dataProviderForHasDevice() {
		return [
			['ff563e501704', true],
			['ffae58501704', true],
			['ffae58591704', true],
			['abcdef123456', false],
		];
	}

	/**
	 * @expectedException \Keepper\MegaD\Exceptions\OneWireDeviceTypeException
	 */
	public function testGetTemperatureWithNotTemperatureSensor() {
		$this->bus->getTemperature('ffae58591704');
	}

	/**
	 * @expectedException \Keepper\MegaD\Exceptions\UnexistsOnewWireDeviceException
	 */
	public function testGetTemperatureWithUnexistsSensor() {
		$this->bus->getTemperature('abcdef123456');
	}

	/**
	 * @dataProvider dataProviderForGetTemperature
	 */
	public function testGetTemperature($uuid, $expected) {
		$sensor = $this->bus->getTemperature($uuid);
		$this->assertEquals($expected, $sensor->getTemperature());
	}

	public function dataProviderForGetTemperature() {
		return [
			['ff563e501704', 7.93],
			['ffae58501704', 4.68]
		];
	}

	public function testStrangeDataOnBus() {
		$this->bus->getDevices();

		$response = new Response();
		$response->getBody()->write('ff563e501704:7.93;ffae58501704:4.68;ffae58591704:0:1');

		$transport = $this->getMockBuilder(MegaDeviceTransportInterface::class)->getMock();
		$transport
			->method('request')
			->willReturn($response);

		$bus = new OneWireBus(new Port(0), $transport);

		try {
			$bus->getDevices();
			$this->assertTrue(false, 'Ожидали исключение');
		} catch (MegaRequestException $e) {
			$this->assertNotNull($e->httpException());
			$this->assertInstanceOf(ResponseFormatException::class, $e->httpException());
		}
	}
}