<?php
namespace Keepper\MegaD\Traits;

trait HtmlParserTrait {

	protected function htmlGetValueFromSelect(string $body, string $name): ?string {
		if ( !preg_match('/\<select[^\>]*name[\'"]?\='.$name.'\>(.*)\<\/select\>/iU', $body, $selectParts) ) {
			$this->logger->debug('Не нашли блок select с именем "'.$name.'"');
			return null;
		}

		if ( !preg_match('/\<option[^\>]*value=([^\s\>]*)[^\>]*selected[^\>]{0,}\>/', $selectParts[1], $optionParts) ) {
			$this->logger->debug('Не нашли блок options selected в блоке select с именем "'.$name.'"');
			return null;
		}

		return $optionParts[1];
	}

}