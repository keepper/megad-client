<?php
namespace Keepper\MegaD\Exceptions;

use Keepper\Lib\Curl\Exceptions\CurlException;

class MegaRequestException extends MedaDeviceException {

	public function httpException(): ?CurlException {
		if ( $this->getPrevious() instanceof CurlException ) {
			return $this->getPrevious();
		}

		return null;
	}
}