<?php
namespace Keepper\MegaD\Device;

use Keepper\Lib\HttpClient\Exceptions\ResponseFormatException;
use Keepper\MegaD\Exceptions\MegaRequestException;
use Keepper\MegaD\Exceptions\OneWireDeviceTypeException;
use Keepper\MegaD\Exceptions\UnexistsOnewWireDeviceException;
use Keepper\MegaD\OneWire\OneWireDevice;
use Keepper\MegaD\OneWire\TemperatureSensor;
use Keepper\MegaD\OneWire\TemperatureSensorInterface;
use Keepper\MegaD\Port\PortInterface;
use Keepper\MegaD\Transport\MegaDeviceTransport;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;

class OneWireBus implements OneWireBusInterface {

	/**
	 * @var MegaDeviceTransportInterface
	 */
	private $transport;

	/**
	 * @var PortInterface
	 */
	private $port;

	public function __construct(
		PortInterface $port,
		MegaDeviceTransportInterface $transport = null
	) {
		$this->port = $port;
		$this->transport = $transport ?? new MegaDeviceTransport();
	}

	/**
	 * @inheritdoc
	 */
	public function getDevices(): array {
		$response = $this->transport->request(['pt' => $this->port->number(), 'cmd' => 'list']);
		$body = trim((string) $response->getBody());

		$sensorsData = explode(';', $body);
		$sensors = [];
		foreach ($sensorsData as $data) {
			$raw = explode(':', $data);

			if (count($raw) == 1) {
				$sensors[] = new OneWireDevice($raw[0]);
				continue;
			} elseif (count($raw) == 2) {
				$sensors[] = new TemperatureSensor($raw[0], $raw[1]);
			} else {
				$message = 'Шина w1 на порту '.$this->port->number().' содержит странные данные w1 датчика '.$data.', '.print_r($raw, true);
				throw new MegaRequestException($message, 0, new ResponseFormatException($message));
			}
		}

		return $sensors;
	}

	/**
	 * @inheritdoc
	 */
	public function hasDevice(string $uuid): bool {
		$devices = $this->getDevices();
		foreach ($devices as $device) {
			if ( $device->uuid() == $uuid ) {
				return true;
			}
		}
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function getTemperature(string $device): TemperatureSensorInterface {
		$devices = $this->getDevices();
		foreach ($devices as $dev) {

			if ( $dev->uuid() != $device ) {
				continue;
			}

			if ( $dev instanceof TemperatureSensorInterface ) {
				return $dev;
			}

			throw new OneWireDeviceTypeException('Устройство w1 uuid:'.$device.' на порту '.$this->port->number().' не является сенсором температуры');
		}

		throw new UnexistsOnewWireDeviceException('Устройство w1 uuid:'.$device.' на порту '.$this->port->number().' не найдено');
	}
}