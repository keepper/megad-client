<?php
namespace Keepper\MegaD\Device;

use Keepper\Lib\HttpClient\Exceptions\ResponseFormatException;
use Keepper\Lib\HttpClient\HttpClient;
use Keepper\Lib\HttpClient\Interfaces\HttpClientInterface;
use Keepper\MegaD\Configuration\ConfigReader;
use Keepper\MegaD\Configuration\ConfigReaderInterface;
use Keepper\MegaD\Exceptions\MegaRequestException;
use Keepper\MegaD\Exceptions\IncorrectPortConfigurationException;
use Keepper\MegaD\Exceptions\UnconfiguredPortException;
use Keepper\MegaD\Exceptions\UnexistingPortException;
use Keepper\MegaD\Port\DsenPortConfiguration;
use Keepper\MegaD\Port\DsenPortConfigurationInterface;
use Keepper\MegaD\Port\InputPortConfiguration;
use Keepper\MegaD\Port\InputPortConfigurationInterface;
use Keepper\MegaD\Port\Port;
use Keepper\MegaD\Port\PortConfiguration;
use Keepper\MegaD\Port\PortConfigurationInterface;
use Keepper\MegaD\Port\PortInterface;
use Keepper\MegaD\Port\PortType;
use Keepper\MegaD\Traits\HtmlParserTrait;
use Keepper\MegaD\Transport\MegaDeviceTransport;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

class MegaDeviceController implements MegaDeviceControllerInterface {

	use LoggerAwareTrait, HtmlParserTrait;

	/**
	 * @var MegaDeviceTransportInterface
	 */
	private $transport;

    /**
     * @var ConfigReaderInterface
     */
	private $cfgReader;

	public function __construct(
	    MegaDeviceTransportInterface $transport = null,
		ConfigReaderInterface $configReader = null
	) {
	    $this->transport = $transport ?? new MegaDeviceTransport();
	    $this->cfgReader = $configReader ?? new ConfigReader(37, $transport);
		$this->setLogger(new NullLogger());
	}

    /**
     * @inheritdoc
     */
    public function configReader(): ConfigReaderInterface {
	    return $this->cfgReader;
    }

	/**
	 * @inheritdoc
	 */
	public function saveState() {
		$this->transport->httpLayer()->withTimeOut(10, 1);
		$this->transport->request(['cmd' => 's']);
	}

	/**
	 * @@inheritdoc
	 */
	public function setTime(\DateTimeInterface $dateTime) {
		$this->transport->httpLayer()->withTimeOut(2, 1);
		$time = $dateTime->format('H:i:s');
		$this->transport->request(['cf' => 7, 'stime' => $time.':0']);
	}

	/**
	 * @@inheritdoc
	 */
	public function runDefaultAction(PortInterface $port) {
		// Проверяем корректность конфигурации
		$this->configReader()->getInputPortConfiguration($port);

		$this->transport->httpLayer()->withTimeOut(10, 1);
		$this->transport->request(['pt' => $port->number(), 'cmd' => 'd']);
	}

	/**
	 * @inheritdoc
	 */
	public function getDeviceTime(): \DateTimeInterface {
		$this->transport->httpLayer()->withTimeOut(2,1);
		$response = $this->transport->request(['cf' => 7]);
		$body = (string) $response->getBody();

		if ( !preg_match('/Cur time\: ([0-9]{1,2}\:[0-9]{1,2}\:[0-9]{1,2})\s/', $body, $parts) ) {
			$message = 'Ошибка определения времени установленное на устройстве';
			$this->logger->error($message . "\n".'Response body: '.$body);
			throw new MegaRequestException($message, 0, new ResponseFormatException($message));
		}

		$curDate = new \DateTime('now');
		return \DateTime::createFromFormat('Y-m-d H:i:s', $curDate->format('Y-m-d '.$parts[1]));
	}

    /**
     * @inheritdoc
     */
    public function getW1bus(int $portNumber): OneWireBusInterface {
        $port = new Port($portNumber);
        $portConfig = $this->configReader()->getDsenPortConfiguration($port);

        if ( !$portConfig->mode()->isW1Bus() ) {
            throw new IncorrectPortConfigurationException('Запрошена шина w1 с порта #'.$portNumber.' не сконфигурированного как шина w1');
        }

        return new OneWireBus($port, $this->transport);
    }
}