<?php
namespace Keepper\MegaD\Device;

use Keepper\MegaD\Configuration\ConfigReaderInterface;
use Keepper\MegaD\Exceptions\MegaRequestException;
use Keepper\MegaD\Exceptions\IncorrectPortConfigurationException;
use Keepper\MegaD\Exceptions\UnconfiguredPortException;
use Keepper\MegaD\Exceptions\UnexistingPortException;
use Keepper\MegaD\Port\PortInterface;

interface MegaDeviceControllerInterface {

    /**
     * Возвращает объект для чтения конфигурации с устройства контроллера
     * @return ConfigReaderInterface
     */
    public function configReader(): ConfigReaderInterface;

	/**
	 * Возвращает время установленое на устройстве.
	 *
	 * Внимание, возвращаемые данные даты (подставляются всегда сервером в значение текущей даты сервера)
	 * @return \DateTimeInterface
	 *
	 * @throws MegaRequestException         В случае ошибки запроса/ответа к устройству
	 */
	public function getDeviceTime(): \DateTimeInterface;

	/**
	 * Говорит контролеру выполнить команду сохранения состояния портов
	 * @return mixed
	 *
	 * @throws MegaRequestException
	 */
	public function saveState();

	/**
	 * @param \DateTimeInterface $dateTime
	 * @return mixed
	 *
	 * @throws MegaRequestException
	 */
	public function setTime(\DateTimeInterface $dateTime);

	/**
	 * Вызов исполнения Act прописанного на Входном порту
	 *
	 * @param PortInterface $port
	 * @return mixed
	 *
	 * @throws UnexistingPortException                  В случае если указанный порт отсутсвует
	 * @throws UnconfiguredPortException                В случае если указанный порт не сконфигурирован
	 * @throws IncorrectPortConfigurationException      В случае если указанный порт сконфигурирован не как "Вход"
	 * @throws MegaRequestException
	 */
	public function runDefaultAction(PortInterface $port);

	/**
	 * Возвращает интерфейс управления шиной w1 на указаном порту
	 * @param int $portNumber
	 * @return OneWireBusInterface
	 *
	 * @throws UnexistingPortException              В случае если указанный порт отсутсвует
	 * @throws UnconfiguredPortException            В случае если указанный порт не сконфигурирован
	 * @throws IncorrectPortConfigurationException  В случае если указанный порт сконфигурирован не как "шина w1"
	 * @throws MegaRequestException                 В случае ошибки запроса/ответа к устройству
	 */
	public function getW1bus(int $portNumber): OneWireBusInterface;

}