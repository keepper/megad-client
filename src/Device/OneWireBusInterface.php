<?php
namespace Keepper\MegaD\Device;

use Keepper\MegaD\Exceptions\MegaRequestException;
use Keepper\MegaD\Exceptions\OneWireDeviceTypeException;
use Keepper\MegaD\Exceptions\UnexistsOnewWireDeviceException;
use Keepper\MegaD\OneWire\OneWireDeviceInterface;
use Keepper\MegaD\OneWire\TemperatureSensorInterface;

interface OneWireBusInterface {

	/**
	 * Возвращает список устройств на шине
	 * @return OneWireDeviceInterface[]
	 *
	 * @throws MegaRequestException     В случае проблем с разбором ответа
	 */
	public function getDevices(): array;

	/**
	 * Возвращает признак наличия на шине указанного устройства
	 * @param string $uuid
	 * @return bool
	 *
	 * @throws MegaRequestException     В случае проблем с разбором ответа
	 */
	public function hasDevice(string $uuid): bool;

	/**
	 * Возвращает указанный сенсор температуры
	 * @param string $device
	 * @return TemperatureSensorInterface
	 *
	 * @throws UnexistsOnewWireDeviceException  Если на шине отсутствуе указанное устройство
	 * @throws OneWireDeviceTypeException       Если указанное устройство не является сенсором температуры
	 * @throws MegaRequestException             В случае проблем с разбором ответа
	 */
	public function getTemperature(string $device): TemperatureSensorInterface;
}