<?php
namespace Keepper\MegaD\Configuration;

use Keepper\MegaD\Exceptions\IncorrectPortConfigurationException;
use Keepper\MegaD\Exceptions\MegaRequestException;
use Keepper\MegaD\Exceptions\UnconfiguredPortException;
use Keepper\MegaD\Exceptions\UnexistingPortException;
use Keepper\MegaD\Port\DsenPortConfigurationInterface;
use Keepper\MegaD\Port\InputPortConfigurationInterface;
use Keepper\MegaD\Port\PortConfigurationInterface;
use Keepper\MegaD\Port\PortInterface;

interface ConfigReaderInterface {
    /**
     * Возвращает данные конфигурации порта.
     * @param PortInterface $port
     * @return PortConfigurationInterface
     *
     * @throws UnexistingPortException      В случае если указанный порт отсутсвует
     * @throws UnconfiguredPortException    В случае если указанный порт не сконфигурирован
     * @throws MegaRequestException         В случае ошибки запроса/ответа к устройству
     */
    public function getPortConfiguration(PortInterface $port): PortConfigurationInterface;

    /**
     * Возвращает конфигурацию указанного порта, как порт сконфигурированый по типу "Входа"
     * @param PortInterface $port
     * @return InputPortConfigurationInterface
     *
     * @throws UnexistingPortException                  В случае если указанный порт отсутсвует
     * @throws UnconfiguredPortException                В случае если указанный порт не сконфигурирован
     * @throws IncorrectPortConfigurationException      В случае если указанный порт сконфигурирован не как "Вход"
     * @throws MegaRequestException                     В случае ошибки запроса/ответа к устройству
     */
    public function getInputPortConfiguration(PortInterface $port): InputPortConfigurationInterface;

    /**
     * Возвращает конфигурацию указанного порта, как порт сконфигурированый по типу "Цифровой сенсор"
     * @param PortInterface $port
     * @return DsenPortConfigurationInterface
     *
     * @throws UnexistingPortException              В случае если указанный порт отсутсвует
     * @throws UnconfiguredPortException            В случае если указанный порт не сконфигурирован
     * @throws IncorrectPortConfigurationException  В случае если указанный порт сконфигурирован не как "Цифровой сенсор"
     * @throws MegaRequestException                 В случае ошибки запроса/ответа к устройству
     */
    public function getDsenPortConfiguration(PortInterface $port): DsenPortConfigurationInterface;
}