<?php
namespace Keepper\MegaD\Configuration;

use Keepper\Lib\HttpClient\Exceptions\ResponseFormatException;
use Keepper\MegaD\Exceptions\IncorrectPortConfigurationException;
use Keepper\MegaD\Exceptions\MegaRequestException;
use Keepper\MegaD\Exceptions\UnconfiguredPortException;
use Keepper\MegaD\Exceptions\UnexistingPortException;
use Keepper\MegaD\Port\DsenPortConfiguration;
use Keepper\MegaD\Port\DsenPortConfigurationInterface;
use Keepper\MegaD\Port\InputPortConfiguration;
use Keepper\MegaD\Port\InputPortConfigurationInterface;
use Keepper\MegaD\Port\PortConfiguration;
use Keepper\MegaD\Port\PortConfigurationInterface;
use Keepper\MegaD\Port\PortInterface;
use Keepper\MegaD\Port\PortType;
use Keepper\MegaD\Traits\HtmlParserTrait;
use Keepper\MegaD\Transport\MegaDeviceTransport;
use Keepper\MegaD\Transport\MegaDeviceTransportInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

class ConfigReader implements ConfigReaderInterface {

    use LoggerAwareTrait, HtmlParserTrait;

    /**
     * @var MegaDeviceTransportInterface
     */
    private $transport;

    private $maxPortNumber;

    public function __construct(
        int $maxPortNumber = 37,
        MegaDeviceTransportInterface $transport = null
    ) {
        $this->maxPortNumber = $maxPortNumber;
        $this->transport = $transport ?? new MegaDeviceTransport();
        $this->setLogger(new NullLogger());
    }

    /**
     * @inheritdoc
     */
    public function getPortConfiguration(PortInterface $port): PortConfigurationInterface {

        if ( $port->number() > $this->maxPortNumber ) {
            throw new UnexistingPortException('Порт №'.$port->number().' не существует');
        }

        $this->transport->httpLayer()->withTimeOut(2, 1);
        $response = $this->transport->request(['pt' => $port->number()]);
        $body = (string) $response->getBody();

        $portType = $this->htmlGetValueFromSelect($body, 'pty');
        if ( is_null($portType) ) {
            $message = 'Ошибка определения типа порта №'.$port->number().' не удалось распарсить ответ от устройства';
            $this->logger->error($message . "\n".'Response body: '.$body);
            throw new MegaRequestException($message, 0, new ResponseFormatException($message));
        }
        $portType = new PortType($portType);
        if ( $portType->isNotConfigured() ) {
            throw new UnconfiguredPortException('Порт №'.$port->number().' не сконфигурирован');
        }

        $result = new PortConfiguration($portType);
        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getInputPortConfiguration(PortInterface $port): InputPortConfigurationInterface {
        $configuration = $this->getPortConfiguration($port);

        if ( !$configuration->type()->isInput() ) {
            $message = 'Запрошена конфигурация порта №'.$port->number().' как "Входного", однако порт сконфигурирован как "'.$configuration->type().'"';
            $this->logger->warning($message);
            throw new IncorrectPortConfigurationException($message);
        }

        $this->transport->httpLayer()->withTimeOut(2, 1);
        $response = $this->transport->request(['pt' => $port->number()]);
        $body = (string) $response->getBody();

        $mode = $this->htmlGetValueFromSelect($body, 'm');
        if ( is_null($mode) ) {
            $message = 'Ошибка определения режима работы порта №'.$port->number().' не удалось распарсить ответ от устройства';
            $this->logger->error($message . "\n".'Response body: '.$body);
            throw new MegaRequestException($message, 0, new ResponseFormatException($message));
        }

        $result = new InputPortConfiguration($mode);
        // @todo Реализовать чтение остальных параметров

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getDsenPortConfiguration(PortInterface $port): DsenPortConfigurationInterface {
        $configuration = $this->getPortConfiguration($port);

        if ( !$configuration->type()->isDigitalSensor() ) {
            $message = 'Запрошена конфигурация порта №'.$port->number().' как "Dsen", однако порт сконфигурирован как "'.$configuration->type().'"';
            $this->logger->warning($message);
            throw new IncorrectPortConfigurationException($message);
        }

        $this->transport->httpLayer()->withTimeOut(2, 1);
        $response = $this->transport->request(['pt' => $port->number()]);
        $body = (string) $response->getBody();

        $mode = $this->htmlGetValueFromSelect($body, 'd');
        if ( is_null($mode) ) {
            $message = 'Ошибка определения режима работы порта №'.$port->number().' не удалось распарсить ответ от устройства';
            $this->logger->error($message . "\n".'Response body: '.$body);
            throw new MegaRequestException($message, 0, new ResponseFormatException($message));
        }

        $result = new DsenPortConfiguration($mode);

        return $result;
    }
}