<?php
namespace Keepper\MegaD\Transport;

use Keepper\Lib\HttpClient\HttpClient;
use Keepper\Lib\HttpClient\Interfaces\ConfiguredHttpClientInterface;
use Keepper\Lib\HttpClient\Interfaces\HttpClientInterface;
use Keepper\MegaD\Exceptions\DeviceIsBusyException;
use Keepper\MegaD\Exceptions\MegaRequestException;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareTrait;
use Psr\Log\NullLogger;

class MegaDeviceTransport implements MegaDeviceTransportInterface {

	use LoggerAwareTrait;

	/**
	 * @var HttpClientInterface
	 */
	private $httpClient;

	private $ip;

	private $pwd;

	private $httpCache = [];

	private $tryCount = 0;

	public function __construct(
		string $ip = '192.168.0.14',
		string $pwd = 'sec',
		HttpClientInterface $httpClient = null
	) {
		$this->ip = $ip;
		$this->pwd = $pwd;
		$this->httpClient = $httpClient ?? new HttpClient();
		$this->setLogger(new NullLogger());
	}

	/**
	 * @inheritdoc
	 */
	public function apiUrl(): string {
		return 'http://'.$this->ip.'/'.$this->pwd.'/';
	}

	/**
	 * @inheritdoc
	 */
	public function httpLayer(): ConfiguredHttpClientInterface {
		return $this->httpClient;
	}

	/**
	 * @inheritdoc
	 */
	public function request(array $params): ResponseInterface {
		$this->logger->debug('Запрос с параметрами '.print_r($params, true));
		$key = md5(print_r([$this->apiUrl(), $params], true));
		if ( array_key_exists($key, $this->httpCache) ) {
			$this->logger->debug('Результат взят из кэша');
			return $this->httpCache[$key];
		}

		try {
			$response = $this->httpClient->get($this->apiUrl(), $params);
		} catch (\Exception $e) {
			$this->logger->debug('Ошибка при исполнении запроса '.$e->getMessage()."\n".$e->getTraceAsString());
			throw new MegaRequestException($e->getMessage(), $e->getCode(), $e);
		}

		$this->logger->debug('Ответ '.$response->getStatusCode().' '.$response->getBody());
		if ( $response->getStatusCode() != 200 ) {
			throw new MegaRequestException('Ответ сервера на команду не 200');
		}

        if (trim((string) $response->getBody()) == 'busy') {
            $this->tryCount++;
            if ($this->tryCount > 10) {
                throw new DeviceIsBusyException('Превышено кол-во попыток чтения при ответе устройства busy');
            }
            usleep(0.1 * 1000000);
            return $this->request($params);
        } else {
            $this->tryCount = 0;
        }

        return $this->httpCache[$key] = $response;
	}
}