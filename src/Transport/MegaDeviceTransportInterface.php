<?php
namespace Keepper\MegaD\Transport;

use Keepper\Lib\HttpClient\Interfaces\ConfiguredHttpClientInterface;
use Keepper\MegaD\Exceptions\MegaRequestException;
use Psr\Http\Message\ResponseInterface;

interface MegaDeviceTransportInterface {

	/**
	 * Возвращает основной URL для Api обращений
	 * @return string
	 */
	public function apiUrl(): string;

	/**
	 * Возвращает внутрений http клиент на базе которого основан транспорт
	 * @return ConfiguredHttpClientInterface
	 */
	public function httpLayer(): ConfiguredHttpClientInterface;

	/**
	 * Выполняет запрос к устройству MegaD по http
	 * @param array $params
	 * @return ResponseInterface
	 *
	 * @throws MegaRequestException При проблемах исполнения запроса
	 */
	public function request(array $params): ResponseInterface;
}