<?php

namespace Keepper\MegaD\OneWire;

interface OneWireDeviceInterface {

	/**
	 * Возвращает уникальный идентификатор устройства
	 * @return string
	 */
	public function uuid(): string;
}