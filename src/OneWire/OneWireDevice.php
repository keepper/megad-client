<?php
namespace Keepper\MegaD\OneWire;

class OneWireDevice implements OneWireDeviceInterface {

	private $uuid;

	public function __construct(string $uuid) {
		$this->uuid = $uuid;
	}

	/**
	 * @inheritdoc
	 */
	public function uuid(): string {
		return $this->uuid;
	}
}