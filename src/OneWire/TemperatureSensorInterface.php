<?php
namespace Keepper\MegaD\OneWire;

interface TemperatureSensorInterface extends OneWireDeviceInterface {

	/**
	 * Возвращает температуру. Если значение на шине еще не сконвертировано, возвращает null
	 * @return float|null
	 */
	public function getTemperature(): ?float;
}