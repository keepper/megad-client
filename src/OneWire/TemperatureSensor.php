<?php
namespace Keepper\MegaD\OneWire;

class TemperatureSensor extends OneWireDevice implements TemperatureSensorInterface {

	private $temperature;

	public function __construct(string $uuid, float $temperature) {
		parent::__construct($uuid);
		$this->temperature = $temperature;
	}

	/**
	 * @inheritdoc
	 */
	public function getTemperature(): ?float {
		$asString = (string) $this->temperature;
		if ( in_array($asString, ['85.0', '85'])  ) {
            return null;
		}

		return $this->temperature;
	}
}