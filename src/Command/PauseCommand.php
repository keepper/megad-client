<?php
namespace Keepper\MegaD\Command;

class PauseCommand extends AbstractCommand implements CommandInterface {
	const CMD = 'p';
	private $delay;

	public function __construct(int $delay) {
		$this->delay = $delay;
	}

	public function getCommand(): string {
		return self::CMD . $this->delay;
	}
}