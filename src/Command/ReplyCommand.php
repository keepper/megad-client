<?php
namespace Keepper\MegaD\Command;

class ReplyCommand extends AbstractCommand implements CommandInterface {
	const CMD = 'r';

	private $count;

	public function __construct(int $replyCount) {
		$this->count = $replyCount;
	}

	public function getCommand(): string {
		return self::CMD . $this->count;
	}
}