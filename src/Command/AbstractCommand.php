<?php
namespace Keepper\MegaD\Command;

abstract class AbstractCommand {

	public function __toString(): string {
		return $this->getCommand();
	}

	abstract public function getCommand(): string;
}