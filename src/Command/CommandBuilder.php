<?php
namespace Keepper\MegaD\Command;

use Keepper\MegaD\Configuration\ConfigReaderInterface;
use Keepper\MegaD\Device\MegaDeviceControllerInterface;
use Keepper\MegaD\Exceptions\BigComandException;
use Keepper\MegaD\Exceptions\NotOutputPortException;
use Keepper\MegaD\Exceptions\UnconfiguredPortException;
use Keepper\MegaD\Exceptions\UnexistingPortException;
use Keepper\MegaD\Port\Port;
use Keepper\MegaD\Port\PortInterface;

class CommandBuilder implements CommandBuilderInterface {

	/**
	 * @var CommandInterface[]
	 */
	private $commands = [];

	/**
	 * @var ConfigReaderInterface
	 */
	private $configReader;

	private $length = 0;

	/**
	 * @inheritdoc
	 */
	public function reply(int $count): CommandBuilderInterface {
		return $this->attachCommand(new ReplyCommand($count));
	}

	public function attachCommand(CommandInterface $command): CommandBuilderInterface {
		$this->checkLength(strlen($command));
		$this->commands[] = $command;
		return $this;
	}

	private function checkLength(int $addedLength) {
		if ($addedLength + 1 + $this->length > 15) {
			throw new BigComandException('Длинна итоговой строки команды превышает допустимую в 15 символов длинну');
		}
		$this->length += $addedLength + 1;
	}

	/**
	 * @inheritdoc
	 */
	public function checkConfiguration(ConfigReaderInterface $configReader): CommandBuilderInterface {
		$this->configReader = $configReader;
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function getResult(): string {
		$commands = [];
		foreach ($this->commands as $command) {
			$commands[] = (string) $command;
		}

		$result = implode(';', $commands);

		if (strlen($result) > 15) {
			throw new BigComandException('Длинна итоговой строки команды превышает допустимую в 15 символов длинну');
		}

		return $result;
	}

	/**
	 * @inheritdoc
	 */
	public function delay(int $delay): CommandBuilderInterface {
		return $this->attachCommand(new PauseCommand($delay));
	}

	protected function throwIfNotOutputPort(PortInterface $port) {
		if ( is_null($this->configReader) ) {
			return;
		}

		if ( !$this->configReader->getPortConfiguration($port)->type()->isOutput() ) {
			throw new NotOutputPortException('Попытка конфигурации команды изменения состяния порта, с указанием порта сконфигурированного не как "Выход". Порт: #'.$port->number());
		}
	}

	/**
	 * @inheritdoc
	 */
	public function turnOn(int $portNumber): CommandBuilderInterface {
		$this->changeOutput($portNumber, OutputCommand::ON);
		return $this;
	}

	protected function changeOutput(int $portNumber, int $state) {
		$port = new Port($portNumber);
		$this->throwIfNotOutputPort($port);
		$this->attachCommand(new OutputCommand($port, $state));
	}

	/**
	 * @inheritdoc
	 */
	public function turnOff(int $portNumber): CommandBuilderInterface {
		$this->changeOutput($portNumber, OutputCommand::OFF);
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function outputToggle(int $portNumber): CommandBuilderInterface {
		$this->changeOutput($portNumber, OutputCommand::TOGGLE);
		return $this;

	}

	/**
	 * @inheritdoc
	 */
	public function allTurnOn(): CommandBuilderInterface {
		return $this->attachCommand(new AllOutputCommand(AllOutputCommand::ON));
	}

	/**
	 * @inheritdoc
	 */
	public function allTurnOff(): CommandBuilderInterface {
		return $this->attachCommand(new AllOutputCommand(AllOutputCommand::OFF));
	}
}