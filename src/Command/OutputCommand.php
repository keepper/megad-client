<?php
namespace Keepper\MegaD\Command;

use Keepper\MegaD\Port\PortInterface;

class OutputCommand extends AbstractCommand implements CommandInterface {

	const OFF = 0;

	const ON = 1;

	const TOGGLE = 2;

	private $cmd;

	/**
	 * @var PortInterface
	 */
	private $port;

	public function __construct(PortInterface $port, int $cmd) {
		$this->port = $port;
		$this->cmd = $cmd;
	}

	public function getCommand(): string {
		return $this->port->number() . ':' . $this->cmd;
	}
}