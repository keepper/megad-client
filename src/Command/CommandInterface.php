<?php
namespace Keepper\MegaD\Command;

interface CommandInterface {

	/**
	 * Алиас @see CommandInterface::getCommand()
	 * @return string
	 */
	public function __toString(): string;

	/**
	 * Возвращает команду в формате который воспринимает контроллер
	 * @return string
	 */
	public function getCommand(): string;
}