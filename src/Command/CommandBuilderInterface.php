<?php
namespace Keepper\MegaD\Command;

use Keepper\MegaD\Configuration\ConfigReaderInterface;
use Keepper\MegaD\Exceptions\BigComandException;
use Keepper\MegaD\Exceptions\NotOutputPortException;
use Keepper\MegaD\Exceptions\UnconfiguredPortException;
use Keepper\MegaD\Exceptions\UnexistingPortException;

interface CommandBuilderInterface {

	/**
	 * Повтор записанного сценария несколько раз.
	 * @param int $count
	 * @return CommandBuilderInterface
	 *
	 * @throws BigComandException Если текущий стек команд привысил общую допустимую длинну в 15 символов
	 */
	public function reply(int $count): self;

	/**
	 * Добавляет команду в стек
	 * @param CommandInterface $command
	 * @return CommandBuilderInterface
	 *
	 * @throws BigComandException Если текущий стек команд привысил общую допустимую длинну в 15 символов
	 */
	public function attachCommand(CommandInterface $command): CommandBuilderInterface;

	/**
	 * Включает указаный выход
	 * @param int $portNumber
	 * @return CommandBuilderInterface
	 *
	 * @throws BigComandException Если текущий стек команд привысил общую допустимую длинну в 15 символов
	 * @throws UnexistingPortException      В случае если указанный порт отсутсвует
	 * @throws UnconfiguredPortException    В случае если указанный порт не сконфигурирован
	 * @throws NotOutputPortException       В случае если указанный сконфигурирован не как "Выход"
	 */
	public function turnOn(int $portNumber): self;

	/**
	 * Выключает указаный выход
	 * @param int $portNumber
	 * @return CommandBuilderInterface
	 *
	 * @throws BigComandException Если текущий стек команд привысил общую допустимую длинну в 15 символов
	 * @throws UnexistingPortException      В случае если указанный порт отсутсвует
	 * @throws UnconfiguredPortException    В случае если указанный порт не сконфигурирован
	 * @throws NotOutputPortException       В случае если указанный сконфигурирован не как "Выход"
	 */
	public function turnOff(int $portNumber): self;

	/**
	 * Включает все порты сконфигурированные как выходы
	 * @return CommandBuilderInterface
	 *
	 * @throws BigComandException Если текущий стек команд привысил общую допустимую длинну в 15 символов
	 * @throws UnexistingPortException      В случае если указанный порт отсутсвует
	 * @throws UnconfiguredPortException    В случае если указанный порт не сконфигурирован
	 * @throws NotOutputPortException       В случае если указанный сконфигурирован не как "Выход"
	 */
	public function allTurnOn(): self;

	/**
	 * Выключает все порты сконфигурированные как выходы
	 * @return CommandBuilderInterface
	 *
	 * @throws BigComandException Если текущий стек команд привысил общую допустимую длинну в 15 символов
	 * @throws UnexistingPortException      В случае если указанный порт отсутсвует
	 * @throws UnconfiguredPortException    В случае если указанный порт не сконфигурирован
	 * @throws NotOutputPortException       В случае если указанный сконфигурирован не как "Выход"
	 */
	public function allTurnOff(): self;

	/**
	 * Меняет состояние выхода на противоположное текущему состоянию
	 * @param int $portNumber
	 * @return CommandBuilderInterface
	 *
	 * @throws BigComandException Если текущий стек команд привысил общую допустимую длинну в 15 символов
	 * @throws UnexistingPortException      В случае если указанный порт отсутсвует
	 * @throws UnconfiguredPortException    В случае если указанный порт не сконфигурирован
	 * @throws NotOutputPortException       В случае если указанный сконфигурирован не как "Выход"
	 */
	public function outputToggle(int $portNumber): self;

	/**
	 * Добавляет задержку
	 * @param int $delay задержка * 0,1 сек
	 * @return CommandBuilderInterface
	 *
	 * @throws BigComandException Если текущий стек команд привысил общую допустимую длинну в 15 символов
	 */
	public function delay(int $delay): self;

	/**
	 * Включает режим проверки конфигурации портов.
	 * @param ConfigReaderInterface $configReader
	 * @return CommandBuilderInterface
	 */
	public function checkConfiguration(ConfigReaderInterface $configReader): self;

	/**
	 * Возвращает результирующую строку команд в формате воспринимаемым MegaD устройством
	 * @return string
	 *
	 * @throws BigComandException Если текущий стек команд привысил общую допустимую длинну в 15 символов
	 */
	public function getResult(): string;
}