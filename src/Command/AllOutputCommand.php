<?php
namespace Keepper\MegaD\Command;

class AllOutputCommand extends AbstractCommand implements CommandInterface {

	const OFF = 0;

	const ON = 1;

	private $cmd;

	public function __construct(int $cmd) {
		$this->cmd = $cmd;
	}

	public function getCommand(): string {
		return 'a:' . $this->cmd;
	}
}