<?php
namespace Keepper\MegaD\Port;

use Keepper\Lib\Enum\Enum;

class DsenPortMode extends Enum implements DsenPortModeInterface {

	/**
	 * @inheritdoc
	 */
	public function isDHT22(): bool {
		return $this->is(self::DHT22);
	}

	/**
	 * @inheritdoc
	 */
	public function isW1(): bool {
		return $this->is(self::W1);
	}

	/**
	 * @inheritdoc
	 */
	public function isW1Bus(): bool {
		return $this->is(self::W1BUS);
	}

	/**
	 * @inheritdoc
	 */
	public function isIButton(): bool {
		return $this->is(self::IB);
	}

	/**
	 * @inheritdoc
	 */
	public function isW26(): bool {
		return $this->is(self::W26);
	}
}