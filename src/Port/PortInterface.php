<?php
namespace Keepper\MegaD\Port;

interface PortInterface {
	/**
	 * Возвращает номер порта
	 * @return int
	 */
	public function number(): int;
}