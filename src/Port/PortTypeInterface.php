<?php
namespace Keepper\MegaD\Port;

use Keepper\Lib\Enum\Interfaces\EnumInterface;

interface PortTypeInterface extends EnumInterface {

	/**
	 * Вход (например, "сухой контакт, выключатели света", U-Sensor, датчик протечки, охранные датчики и т.д.)
	 */
	const IN = '0';

	/**
	 * Выход (например, включение электроприборов)
	 */
	const OUT = '1';

	/**
	 * Цифровой датчик (например, датчики температуры DS18B20, температуры-влажности DHT22, считыватели iButton, Wiegand-26)
	 */
	const DSEN = '3';

	/**
	 * АЦП, аналого-цифровой преобразователь (например, подключение аналоговых датчиков освещенности, давления, газа и т.д.) Доступен не для всех портов.
	 */
	const ADC = '2';

	/**
	 * Порт от шины I2C
	 */
	const I2C = '4';

	/**
	 * Не сконфигурированный порт
	 */
	const NC = '255';

	/**
	 * Является ли тип порта "Входом"
	 * @see PortTypeInterface::IN
	 * @return bool
	 */
	public function isInput(): bool;

	/**
	 * Является ли тип порта "Выходом"
	 * @see PortTypeInterface::OUT
	 * @return bool
	 */
	public function isOutput(): bool;

	/**
	 * Является ли тип порта "Цифровой датчик"
	 * @see PortTypeInterface::DSEN
	 * @return bool
	 */
	public function isDigitalSensor(): bool;

	/**
	 * Является ли тип порта "АЦП"
	 * @see PortTypeInterface::ADC
	 * @return bool
	 */
	public function isAnalogDigitalConverter(): bool;

	/**
	 * Используется как часть шины I2C
	 * @see PortTypeInterface::I2C
	 * @return bool
	 */
	public function isI2C(): bool;

	/**
	 * Порт не сконфигурирован
	 * @see PortTypeInterface::NC
	 * @return bool
	 */
	public function isNotConfigured(): bool;
}