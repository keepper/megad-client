<?php
namespace Keepper\MegaD\Port;

class DsenPortConfiguration extends PortConfiguration implements DsenPortConfigurationInterface {

	/**
	 * @var DsenPortModeInterface
	 */
	private $mode;

	public function __construct(string $mode) {
		parent::__construct(PortTypeInterface::DSEN);
		$this->mode = new DsenPortMode($mode);
	}

	/**
	 * @inheritdoc
	 */
	public function mode(): DsenPortModeInterface {
		return $this->mode;
	}
}