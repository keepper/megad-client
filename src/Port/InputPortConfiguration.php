<?php
namespace Keepper\MegaD\Port;

class InputPortConfiguration extends PortConfiguration implements InputPortConfigurationInterface {

	/**
	 * @var InputPortModeInterface
	 */
	private $mode;

	/**
	 * @var string
	 */
	private $action;

	/**
	 * @var string
	 */
	private $net;

	private $isAlwaysP_R;

	private $isAlwaysCallAction;

	private $isAlwaysCallPost;

	private $offBounceProtection;

	public function __construct(
		string $mode,
		string $action = '',
		string $net = '',
		bool $isAlwaysP_R = false,
		bool $isAlwaysCallAction = false,
		bool $isAlwaysCallPost = false,
		bool $offBounceProtection = false
	) {
		parent::__construct(PortTypeInterface::IN);
		$this->mode = new InputPortMode($mode);
		$this->action = $action;
		$this->net = $net;

		$this->isAlwaysP_R = $isAlwaysP_R;
		$this->isAlwaysCallAction = $isAlwaysCallAction;
		$this->isAlwaysCallPost = $isAlwaysCallPost;
		$this->offBounceProtection = $offBounceProtection;
	}

	/**
	 * @inheritdoc
	 */
	public function mode(): InputPortModeInterface {
		return $this->mode;
	}

	/**
	 * @inheritdoc
	 */
	public function action(): string {
		return $this->action;
	}

	/**
	 * @inheritdoc
	 */
	public function isAlwaysCallAction(): bool {
		return $this->isAlwaysCallAction;
	}

	/**
	 * @inheritdoc
	 */
	public function postNetworkRequestAction(): string {
		return $this->net;
	}

	/**
	 * @inheritdoc
	 */
	public function isAlwaysCallPost(): bool {
		return $this->isAlwaysCallPost;
	}

	/**
	 * @inheritdoc
	 */
	public function isAlwayseP_R(): bool {
		return $this->isAlwaysP_R;
	}

	/**
	 * @inheritdoc
	 */
	public function isOffBounceProtection(): bool {
		return $this->offBounceProtection;
	}
}