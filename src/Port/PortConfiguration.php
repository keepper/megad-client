<?php
namespace Keepper\MegaD\Port;

class PortConfiguration implements PortConfigurationInterface {

	/**
	 * @var PortTypeInterface
	 */
	private $type;

	public function __construct(string $type) {
		$this->type = new PortType($type);
	}

	/**
	 * @inheritdoc
	 */
	public function type(): PortTypeInterface {
		return $this->type;
	}
}