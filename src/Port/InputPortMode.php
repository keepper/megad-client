<?php
namespace Keepper\MegaD\Port;

use Keepper\Lib\Enum\Enum;

class InputPortMode extends Enum implements InputPortModeInterface {

	/**
	 * @inheritdoc
	 */
	public function isPress(): bool {
		return $this->is(self::PRESS);
	}

	/**
	 * @inheritdoc
	 */
	public function isRelease(): bool {
		return $this->is(self::RELEASE);
	}

	/**
	 * @inheritdoc
	 */
	public function isPressAndRelease(): bool {
		return $this->is(self::PRESS_AND_RELEASE);
	}

	/**
	 * @inheritdoc
	 */
	public function isClick(): bool {
		return $this->is(self::CLICK);
	}
}