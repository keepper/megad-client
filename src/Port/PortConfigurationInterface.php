<?php
namespace Keepper\MegaD\Port;

interface PortConfigurationInterface {

	/**
	 * IN
	 *  ACT + checkbox
	 *  NET + checkbox
	 *  MODE + checkbox (P, R, P&R, C)
	 *  RAW + checkbox
	 *
	 * OUT
	 *  Default
	 *  Mode (SW, PWM, SW_LINK, DS2413)
	 *  Group
	 */

	/**
	 * Тип порта
	 * @return PortTypeInterface
	 */
	public function type(): PortTypeInterface;
}