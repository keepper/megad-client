<?php
namespace Keepper\MegaD\Port;

use Keepper\Lib\Enum\Interfaces\EnumInterface;

interface InputPortModeInterface extends EnumInterface {

	/**
	 * устройство реагирует (то есть отправляет сообщения на сервер, выполняет сценарии и т.д.)
	 * только при замыкании контакта/выключателя
	 */
	const PRESS = '0';

	/**
	 * устройство реагирует только при размыкании контакта/выключателя.
	 * На сервер отправляется дополнительный параметр "m=1". (Пример: http://192.168.0.1/megad.php?pt=4&m=1)
	 */
	const RELEASE = '2';

	/**
	 * устройство реагирует как на замыкание, так и на размыкания контакта.
	 */
	const PRESS_AND_RELEASE = '1';

	/**
	 * Click Mode
	 * При однократном нажатии на выключатель на сервер передается параметр click=1 (Пример: /md.php?pt=0&click=1&cnt=1)
	 * При двойном нажатии (двойной клик) на сервер передается параметр click=2 (Пример: /md.php?pt=0&click=2&cnt=2)
	 * При удержатии клавиши как и в других режимах передается параметр m=2 (Пример: /md.php?pt=0&m=2&cnt=3)
	 * После отпускания клавиши после длительного нажатия передается параметр m=1 (как в режиме P&R) (Пример: /md.php?pt=0&m=1&cnt=3)
	 * Есть изменения и в работе сценария по умолчанию (Action).
	 * Теперь допустимо написать так: 7:2|8:2
	 * Это означает, что при одинарном клике выполнится 7:2, а при двойном 8:2
	 * Внимание: Необходимо отметить, что в случае одинарного клика (в режиме "C") информация на сервер
	 * (или выполнение сценария) происходит с задержкой в 500 мс, которая требуется для фиксации двойного клика.
	 */
	const CLICK = '3';

	/**
	 * Является ли режим режимом @see InputPortModeInterface::PRESS
	 * @return bool
	 */
	public function isPress(): bool;

	/**
	 * Является ли режим режимом @see InputPortModeInterface::RELEASE
	 * @return bool
	 */
	public function isRelease(): bool;

	/**
	 * Является ли режим режимом @see InputPortModeInterface::PRESS_AND_RELEASE
	 * @return bool
	 */
	public function isPressAndRelease(): bool;

	/**
	 * Является ли режим режимом @see InputPortModeInterface::CLICK
	 * @return bool
	 */
	public function isClick(): bool;
}