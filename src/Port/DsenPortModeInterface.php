<?php
namespace Keepper\MegaD\Port;

use Keepper\Lib\Enum\Interfaces\EnumInterface;

interface DsenPortModeInterface extends EnumInterface {

	/**
	 * Режим работы сенсора DHT22
	 */
	const DHT22 = '1';

	/**
	 * Режим работы W1
	 */
	const W1 = '3';

	/**
	 * Режим работы w1 шины
	 */
	const W1BUS = '5';

	/**
	 * Режим работы iButton
	 */
	const IB = '4';

	/**
	 * Режим работы w26
	 */
	const W26 = '6';

	/**
	 * @see DsenPortModeInterface::DHT22
	 * @return bool
	 */
	public function isDHT22(): bool;

	/**
	 * @see DsenPortModeInterface::W1
	 * @return bool
	 */
	public function isW1(): bool;

	/**
	 * @see DsenPortModeInterface::W1BUS
	 * @return bool
	 */
	public function isW1Bus(): bool;

	/**
	 * @see DsenPortModeInterface::IB
	 * @return bool
	 */
	public function isIButton(): bool;

	/**
	 * @see DsenPortModeInterface::W26
	 * @return bool
	 */
	public function isW26(): bool;
}