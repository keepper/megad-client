<?php
namespace Keepper\MegaD\Port;

use Keepper\Lib\Enum\Enum;

class PortType extends Enum implements PortTypeInterface {

	/**
	 * @inheritdoc
	 */
	public function isInput(): bool {
		return $this->is(self::IN);
	}

	/**
	 * @inheritdoc
	 */
	public function isOutput(): bool {
		return $this->is(self::OUT);
	}

	/**
	 * @inheritdoc
	 */
	public function isDigitalSensor(): bool {
		return $this->is(self::DSEN);
	}

	/**
	 * @inheritdoc
	 */
	public function isAnalogDigitalConverter(): bool {
		return $this->is(self::ADC);
	}

	/**
	 * @inheritdoc
	 */
	public function isI2C(): bool {
		return $this->is(self::I2C);
	}

	/**
	 * @inheritdoc
	 */
	public function isNotConfigured(): bool {
		return $this->is(self::NC);
	}
}