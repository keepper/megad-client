<?php
namespace Keepper\MegaD\Port;

interface DsenPortConfigurationInterface extends PortConfigurationInterface {

	/**
	 * Возвращает режим работы порта
	 * @return DsenPortModeInterface
	 */
	public function mode(): DsenPortModeInterface;
}