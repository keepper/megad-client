<?php
namespace Keepper\MegaD\Port;

class Port implements PortInterface {

	private $number;

	public function __construct(int $number) {
		$this->number = $number;
	}

	/**
	 * @inheritdoc
	 */
	public function number(): int {
		return $this->number;
	}
}